import 'package:flutter/material.dart';
import 'package:flutter_fortune_wheel_example/pages/wheel.dart';

void main() {
  runApp(DemoApp());
}

class DemoApp extends StatefulWidget {
  @override
  _DemoAppState createState() => _DemoAppState();
}

class _DemoAppState extends State<DemoApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: FortuneWheelPage());
  }
}
