import 'package:flutter/material.dart';
import 'package:flutter_fortune_wheel_example/widgets/roll_button.dart';
import '../lib/src/core/core.dart';
import 'package:flutter_fortune_wheel_example/lib/src/indicators/indicators.dart';
import 'package:flutter_fortune_wheel_example/lib/src/wheel/wheel.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:go_router/go_router.dart';

import '../common/common.dart';

class FortuneWheelPage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final selected = useStreamController<int>();
    final selectedIndex = useStream(selected.stream, initialData: 0).data ?? 0;
    final isAnimating = useState(false);

    void handleRoll() {
      selected.add(
        roll(Constants.fortuneValues.length),
      );
    }

    return Scaffold(
      body: Container(
        width: double.maxFinite,
        color: Colors.green,
        child: Column(
          children: [
            RollButtonWithPreview(
              selected: selectedIndex,
              items: Constants.fortuneValues,
              onPressed: isAnimating.value ? null : handleRoll,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.85,
              height: 450,
              child: FortuneWheel(
                  alignment: Alignment.topCenter,
                selected: selected.stream,
                onAnimationStart: () => isAnimating.value = true,
                onAnimationEnd: () => isAnimating.value = false,
                onFling: handleRoll,
                hapticImpact: HapticImpact.heavy,
                indicators: [
                  FortuneIndicator(
                    alignment: Alignment.topCenter,
                    child: Image.asset('assets/images/triangle.png'),
                  ),
                ],
                items: [
                  ...List.generate(
                      Constants.fortuneValues.length,
                      (index) => FortuneItem(
                          style: FortuneItemStyle(
                              color: index % 2 == 0 ? Color(0xFFD70304) : Color(0xFFEBD9CA), borderColor: index % 2 == 0 ? Color(0xFFD70304) : Color(0xFFEBD9CA), borderWidth: 10),
                          child: RotatedBox(
                              quarterTurns: 1,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.network(
                                    'https://hoang-phuc.com/media/wysiwyg/768x370.png',
                                    height: 20,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    width: 60,
                                    child: Text(
                                      "LỜI CHÚC NĂM MỚI",
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              )),
                          onTap: () => print("${Constants.fortuneValues[index]}"))).toList(),
                ],
              ),
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}
