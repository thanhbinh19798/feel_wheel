part of 'wheel.dart';

/// Draws a slice of a circle. The slice's arc starts at the right (3 o'clock)
/// and moves clockwise as far as specified by angle.
class _CircleSlicePainter extends CustomPainter {
  final Color fillColor;
  final Color? strokeColor;
  final double strokeWidth;
  final double angle;

  // final int index;
  // final int itemCount;

  const _CircleSlicePainter({
    required this.fillColor,
    this.strokeColor,
    this.strokeWidth = 1,
    this.angle = _math.pi / 2,
    // int index, int itemCount
  }) : assert(angle > 0 && angle < 2 * _math.pi);

  @override
  void paint(Canvas canvas, Size size) {
    final radius = _math.min(size.width - 20, size.height - 20);
    final path = _CircleSlice.buildSlicePath(radius, angle);
    final strokeBorderWidth = 2.0;
    // fill slice area
    canvas.drawPath(
      path,
      Paint()
        ..color = fillColor
        ..style = PaintingStyle.fill,
    );

    // draw slice border
    if (strokeWidth > 0) {
      canvas.drawPath(
        path,
        Paint()
          ..color = Colors.yellow
          ..strokeWidth = strokeBorderWidth
          ..style = PaintingStyle.stroke,
      );

      canvas.drawPath(
        Path()
          ..arcTo(
              Rect.fromCircle(
                center: Offset(0, 0),
                radius: radius,
              ),
              0,
              angle,
              false),
        Paint()
          ..color = strokeColor!
          // .withOpacity(opacity)
          ..strokeWidth = strokeWidth/2
          ..style = PaintingStyle.stroke,
      );
      // shadow trong
      final gradientInside = RadialGradient(
        colors: [
          Colors.transparent,
          Colors.transparent,
          Colors.transparent,
          Colors.black.withOpacity(0.3),
        ], // Adjust the colors as needed
        stops: [0.0, 0.2, 0.9, 1],
      );
      final paint = Paint()
        ..strokeWidth = 5.0 // Set the width of the shadow
        ..style = PaintingStyle.fill
        ..shader = gradientInside.createShader(Rect.fromCircle(center: Offset(0, 0), radius: radius - strokeWidth)); // Set the shader with the linear gradient
      canvas.drawCircle(Offset(0, 0), radius - strokeWidth, paint);

      final numberOfDots = 4; // Adjust as needed
      double angleBetweenDots = angle / (numberOfDots - 1);

      // Draw dots along the border of the arc
      for (int i = 0; i < numberOfDots; i++) {
        double dotAngle = i * angleBetweenDots;
        double dotX = (radius + strokeBorderWidth  - strokeWidth/2)  * cos(dotAngle);
        double dotY = (radius + strokeBorderWidth  - strokeWidth/2) * sin(dotAngle);

        canvas.drawCircle(
          Offset(dotX, dotY),
          strokeWidth/2, // Adjust dotRadius as needed
          Paint()
            ..color = Color(0XFFFFFFFF) // Adjust dotColor as needed
            ..style = PaintingStyle.fill,
        );
      }

      // đèn
      final gradient = RadialGradient(
        colors: [Colors.white, Colors.yellow.withOpacity(0.1)], // Adjust the colors as needed
        stops: [0.0, 1.0],
      );
      for (var i = 0; i < numberOfDots; i++) {
        double dotAngle = i * angleBetweenDots;
        double dotX = (radius + strokeBorderWidth  - strokeWidth/2)  * cos(dotAngle);
        double dotY = (radius + strokeBorderWidth - strokeWidth/2) * sin(dotAngle);

        canvas.drawCircle(
            Offset(dotX, dotY),
            strokeWidth / 2 + 10, // Adjust dotRadius as needed
            Paint()
              // ..color = Colors.yellow.withOpacity(0.1) // Adjust dotColor as needed
              ..style = PaintingStyle.fill
              ..strokeWidth = 2
              ..shader = gradient.createShader(Rect.fromCircle(center: Offset(dotX, dotY), radius: strokeWidth / 2 + 10)));
      }
    }
  }

  @override
  bool shouldRepaint(_CircleSlicePainter oldDelegate) {
    return angle != oldDelegate.angle || fillColor != oldDelegate.fillColor || strokeColor != oldDelegate.strokeColor || strokeWidth != oldDelegate.strokeWidth;
  }
}
